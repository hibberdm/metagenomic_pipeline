#!/bin/bash

################################################################################
# SCRIPT NAME : bowtie_host_filter_array.sbatch                                #
# DESCRIPTION : bowtie-based host filtering                                    #
# ARGS        : SID.list, config.sh                                            #
# AUTHOR      : Matthew C. Hibberd and Daniel M. Webber                        #
# EMAIL       : hibberdm@wustl.edu                                             #
# UPDATED     : 10/10/2023                                                     #
################################################################################

#SBATCH --mem=30G
#SBATCH --cpus-per-task=8

set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass SID.list"
    exit 1
fi

if [ -z "${2}" ]; then
    echo "[ERROR] Must pass the config file"
    exit 1
else
    CONFIG_LOC=$2
fi

source $CONFIG_LOC

METADATA=$( sed -ne '/^#/!p' "${1}" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

IFS=$'\t' read -r RUN_DIR ID SID SUBGROUP PLATFORM HOST <<< "$METADATA"
unset IFS

if [ $PLATFORM = 'novaseq_x_plus' ]; then
        NUM_LANES=8
else
        NUM_LANES=4
fi

mkdir -p "${HF_DIR}/${RUN_DIR}"

eval $( spack load --sh bowtie2/kgu6znd ) # 2.3.4.1, DMW Updated
eval $( spack load --sh samtools/zv3tvj2 )

if [ "$HOST" == "human" ]; then
  HOST_DB=${HOST_DB_HUMAN}
elif [ "$HOST" == "mouse" ]; then
  HOST_DB=${HOST_DB_MOUSE}
elif [ "$HOST" == "pig" ]; then
  HOST_DB=${HOST_DB_PIG}
else
  echo "[ERROR] Host species ${HOST} not recognized!"
  exit 1
fi

VALIDATE=false

counter=0

for ((i = 0; i < $NUM_LANES; ++i)); do
  LANE=$(( i + 1 ))

  if [[ -f "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_1.fq.gz" &&
  -f "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_2.fq.gz" ]]; then

    echo "[STATUS] Beginning host filtering for ${ID}_${LANE}."

    bowtie2 \
      -p "${SLURM_CPUS_PER_TASK}" \
      -1 "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_1.fq.gz" \
      -2 "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_2.fq.gz" \
      -x "${HOST_DB}" \
      --un-conc-gz "${HF_DIR}/${RUN_DIR}/${SID}_${LANE}_HF.fq.gz" \
      | samtools view -hbSF4 - > "${HF_DIR}/${RUN_DIR}/${SID}_${LANE}_HF.bam"

    counter=$((counter+1))

  else
    echo "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_1.fq.gz or \
    ${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_2.fq.gz not found."

  fi

	if [ "$VALIDATE" == true ]; then

    echo '[STATUS] Validating read counts for trimmed fastq files'

	  R1_read_ct=`echo $(zcat "${HF_DIR}/${RUN_DIR}/${SID}_${LANE}_HF.fq.1.gz"|wc -l)/4|bc`
	  R2_read_ct=`echo $(zcat "${HF_DIR}/${RUN_DIR}/${SID}_${LANE}_HF.fq.2.gz"|wc -l)/4|bc`

    if [ "$R1_read_ct" -eq "$R2_read_ct" ]; then
	    echo "[QC passed] R1 and R2 from host-filtered fastq files have an equal number of reads."
    else
	    echo "[ERROR] The number of reads in the following host-filtered fastq files do not match ${SID}_${LANE}_val_1.fq.gz, ${SID}_${LANE}_val_1.fq.gz"
   	  exit 1
    fi
  fi

done

if ! ((counter > 0)); then
    echo "[ERROR] No fastq.gz files found for SID ${SID} in ${QC_DIR}/${RUN_DIR}!"
    exit 1
fi

#echo "[STATUS] Host filtering complete. Removing corresponding files in ${QC_DIR}."

#rm "${QC_DIR}"/"${RUN_DIR}"/"${SID}"*.fq.gz

#echo "[STATUS] File removal complete."
