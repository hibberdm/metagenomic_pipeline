#!/usr/bin/env bash

################################################################################
# SCRIPT NAME : metaphlan2.sbatch                                                #
# DESCRIPTION : this script generates taxonomic profiles from metagenomics     #
#             : data using mOTU_v2 (Bork Lab)                                  #
# ARGS        : SID list (positional)                                          #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
################################################################################

#SBATCH --mem=20G
#SBATCH --cpus-per-task=8

set -e

ml metagenomics_pipeline/3.0
ml metaphlan2/2.6.0

source vars.sh

mkdir -p "${TAX_DIR}"

SID=$( sed -ne '/^#/!p' "${1}" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

prefix="NZ_"${SID}

metaphlan2.py \
    "${HF_DIR}"/concatenated/"${SID}"_HF_CAT_R1.fq.gz,"${HF_DIR}"/concatenated/"${SID}"_HF_CAT_R2.fq.gz \
    --sample_id "${SID}" \
    --bowtie2out "${TAX_DIR}/${prefix}_bowtie2.bz2" \
    --input_type fastq \
    --nproc ${SLURM_CPUS_PER_TASK} \
    > "${TAX_DIR}/${prefix}_metaphlan2_profile.txt"

