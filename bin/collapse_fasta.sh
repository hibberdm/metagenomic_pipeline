#!/bin/bash
# Written by Daniel Webber, 10/25/2023
# Usage: ./collapse_fasta.sh -d [input_directory] -o [output_directory] -e [file_extension] -z
# To run the script without gzipped output, simply omit the -z; to gzip the output include the -z option
# Notes: this will work on non-zipped or gz files, as indicated in the file extension.

# Command-line options
while getopts ":d:o:e:z" opt; do
  case $opt in
    d) input_dir="$OPTARG";;
    o) output_dir="$OPTARG";;
    e) ext="$OPTARG";;
    z) gzip_output=true;;
    \?) echo "Invalid option -$OPTARG" >&2; exit 1;;
    :) echo "Option -$OPTARG requires an argument." >&2; exit 1;;
  esac
done

# Default values if not specified
input_dir=${input_dir:-"./"}
output_dir=${output_dir:-"collapsed"}
ext=${ext:-".fna"}

mkdir -p "$output_dir"

for fasta_file in "$input_dir"/*"$ext" "$input_dir"/*"$ext".gz; do
    base=$(basename -- "$fasta_file")
    base_no_ext="${base%.*}"
    if [[ "$fasta_file" == *.gz ]]; then
        base_no_ext="${base_no_ext%.*}"
        read_cmd="zcat"
    else
        read_cmd="cat"
    fi
    output_file="$output_dir/${base_no_ext}_collapsed.fna"

    if [ "$gzip_output" = true ]; then
        output_file="$output_file.gz"
    fi

    # Check if output file already exists
    if [ -e "$output_file" ]; then
        echo "Output file $output_file already exists. Skipping."
        continue
    fi

    if [ "$gzip_output" = true ]; then
        gzip_cmd="gzip"
    else
        gzip_cmd="cat"
    fi

    echo ">${base_no_ext}" | $gzip_cmd > "$output_file"

    $read_cmd "$fasta_file" | awk 'BEGIN {ORS=""; first=1} /^#/ {next} /^>/ {if (first) {first=0} else {printf "NNNNNNNNNN"}} !/^>/ {print}' | $gzip_cmd >> "$output_file"
    echo "" | $gzip_cmd >> "$output_file"
done

