#!/bin/bash

################################################################################
# SCRIPT NAME : trim_galore_array.sbatch                                       #
# DESCRIPTION : this script performs quality and adapter trimming              #
# ARGS        : mapping file (positional, required)                            #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
# UPDATED     : October 11, 2023.                                              #
################################################################################

#SBATCH --mem=1G
#SBATCH --cpus-per-task=2

set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass the mapping file"
    exit 1
fi

if [ -z "${2}" ]; then
    echo "[ERROR] Must pass the config file"
    exit 1
else
    CONFIG_LOC=$2
fi

source $CONFIG_LOC

VALIDATE=false

METADATA=$( sed -ne '/^#/!p' "${1}" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

IFS=$'\t' read -r RUN_DIR ID SID SUBGROUP PLATFORM HOST <<< "$METADATA"
unset IFS

mkdir -p  "${QC_DIR}/${RUN_DIR}"

eval $( spack load --sh trimgalore/3aakrnu )

counter=0

for ((i = 0; i < 8; ++i)); do
    LANE=$(( i + 1 ))

    if [[ -f "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R1.fq.gz" &&
    -f "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R2.fq.gz" ]]; then

        trim_galore \
            --cores "${SLURM_CPUS_PER_TASK}" \
            --phred33 \
            --length 50 \
            -e 0.1 \
            --2colour 20 \
            --paired \
            --basename "${SID}_${LANE}" \
            --output_dir "${QC_DIR}/${RUN_DIR}" \
            --trim-n \
            --retain_unpaired \
            --length_1 75 \
            --length_2 75 \
            "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R1.fq.gz" \
            "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R2.fq.gz"
 
   counter=$((counter+1))

    else
        echo "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R1.fq.gz or \
             ${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R2.fq.gz not found." 

    fi

    if [ "$VALIDATE" == true ]; then

	    echo '[STATUS] Validating trimmed fastq files'

	    R1_read_ct=`echo $(zcat "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_1.fq.gz"|wc -l)/4|bc`
	    R2_read_ct=`echo $(zcat "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_2.fq.gz"|wc -l)/4|bc`

	    if [ "$R1_read_ct" -eq "$R2_read_ct" ]; then
    		echo "[QC passed] The trimmed fastq files have an equal number of reads."
		else
	    	echo "[ERROR] The number of reads in the following two trimmed fastq files do not match: ${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_1.fq.gz, ${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_2.fq.gz"
		    exit 1
        fi
	fi

done

if ! ((counter > 0)); then
    echo "[ERROR] No fastq.gz files found for ID ${ID} in ${READS_DIR}/${RUN_DIR}!"
    exit 1
fi
