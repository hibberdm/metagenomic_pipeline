#!/bin/bash

################################################################################
# SCRIPT NAME : cleanup.sbatch                                                 #
# DESCRIPTION : this script cleans up unnecessary intermediate files           #
# ARGS        : none                                                           #
# REVISED     : 230105                                                         #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
################################################################################

rm 01_quality_control/*/*
rm 02_host_filtered/*/*.gz
rm 03_assembly/*/*_numbered.fa
rm -r 03_assembly/*/intermediate_contigs/
rm 04_annotation/*/*.kallisto
rm 06_contig_quantification/*/pseudoalignments.bam


set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass the config file"
    exit 1
else
    CONFIG_LOC=$1
fi

source $CONFIG_LOC

# Remove trimmed and quality-filtered reads
rm ${QC_DIR}/*/*.fq.gz
rm 01_quality_control/*/*

# Remove host-filtered data (fastq and bam files)
rm ${HF_DIR}/*/*.fq.*.gz
rm ${HF_DIR}/*/*.bam
rm 02_host_filtered/*/*.gz

# Remove merged data (used as input to megahit assembler)
rm ${HF_DIR}/concatenated/*.fq.gz

# Remove extraneous assembly files (essentially all of the kmer assemblies other than the "best" one)
rm -r ${ASSEMBLY_DIR}/*/intermediate_contigs
rm 03_assembly/*/*_numbered.fa
rm -r 03_assembly/*/intermediate_contigs/

# Remove kallisto indices
rm 04_annotation/*/*.kallisto

# Remove bam files from re-mapping/counting
rm ${COUNTS_DIR}/*.bam

# Remove raw data - make optional
rm ${READS_DIR}/*/*.fastq.gz
rm ${READS_DIR}/*/*.fq.gz
