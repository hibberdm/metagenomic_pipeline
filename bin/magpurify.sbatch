#!/bin/bash

################################################################################
# SCRIPT NAME : magpurify.sbatch                                               #
# DESCRIPTION : this script performs sequential mag refinement steps           #
# ARGS        : 1) list of full paths of input mags (ex: from das_tool)        #
#             : 2) config file, containing assembly workflow defaults          #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
# REVISION    : July 22, 2022                                                  #
################################################################################

#SBATCH --mem=2G
#SBATCH --cpus-per-task=1
#SBATCH --output logs/mag/slurm-magpurify-%A_%a.out

set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass the config file"
    exit 1
else
    CONFIG_LOC=$1
fi

source $CONFIG_LOC

eval $(spack load --sh py-magpurify/hkcvn6y) #v2.1.2

export MAGPURIFYDB="/ref/jglab/data/magpurify/1.0/"

MAG=$( sed -ne '/^#/!p' "${WORK_DIR}/MAGS_FOR_REFINEMENT.list" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

MAG_OUT=$(basename $MAG ".fa")
TMP_DIR=$(dirname $MAG)/${MAG_OUT}_magpurify

mkdir -p $MAG_REFINEMENT_DIR
mkdir -p ${MAG_REFINEMENT_DIR}/refined_mags
mkdir -p ${TMP_DIR}

magpurify \
    phylo-markers \
    --threads $SLURM_CPUS_PER_TASK \
    $MAG \
    $TMP_DIR

magpurify \
    clade-markers \
    --threads $SLURM_CPUS_PER_TASK \
    $MAG \
    $TMP_DIR

magpurify \
    tetra-freq \
    $MAG \
    $TMP_DIR

magpurify \
    gc-content \
    $MAG \
    $TMP_DIR

magpurify \
    clean-bin \
    $MAG \
    $TMP_DIR \
    ${MAG_REFINEMENT_DIR}/refined_mags/${MAG_OUT}.mp.fa

wait

if [ -e "${MAG_REFINEMENT_DIR}/refined_mags/${MAG_OUT}.mp.fa" ]; then
    rm -r $TMP_DIR
fi

